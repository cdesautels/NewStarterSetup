# Exercise #

Your goal is to get expposed to the technology stack we are using by completing the follow exercise:

Consume messages from the **brLivescoutRawFixtures** Kafka topic and teams (1 and 2) from the fixture to their William Hill competitor equivalent in the **fixture-mapping-service**, if a message does not contain teams ignore it. If all teams in a single message can be correlated publish this small subset of information to a new topic **brLivescoutMappedCompetitors**.

Solve this issue using the following recommended libraries (these are already present in the provided pom.xml):
* Spring Webflux - application framework
* Caffiene - general purpose caching
* Immutables - domain objects

Please use unit and integration tests with realistic data to demonstrate the behaviour. 

You may use this folder as a starting point for your project.

## Examples ##
Interacting with the pre-production Kafka cluster:
* Simple Kafka rest service Swagger documenation: http://simple-kafka-rest-service.trading-api.pp1.williamhill.plc/swagger-ui.html#/
* Simple Kafka rest service API: http://simple-kafka-rest-service.trading-api.pp1.williamhill.plc/topics/brLivescoutRawFixtures
* Using the Kafka console consumer from the pre-production jump host: **brs-ncde-linux-jump-01.nonprod.williamhill.plc**

Interacting with the pre-production fixture-mapping-service:
* Fixture-mapping-service documation: https://conf.willhillatlas.com/display/TRAD/Fixture+Mapping+Service 
* Fixture-mapping-service API example: http://platform-fixture-mapping-service.trading-api.pp1.williamhill.plc/competitors/byFeed/bet_radar_lcoo/6679349

Additionally, the samples folder contains a zip of all messages (at 01/07/2019) from the **brLivescoutRawFixtures** topic and a response from the **fixture-mapping-service** from the above query.

## Optional ## 
Implement Spring Cloud Contracts.

# Reading #
* [Service Architecture](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/)
* [Spring Webflux](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html)
* [Spring Cloud Contract](https://spring.io/projects/spring-cloud-contract)
* [Kafka Reactor](https://projectreactor.io/docs/kafka/release/reference/)
* [Immutables](https://immutables.github.io/)
* [Trading Diagrams](https://conf.willhillatlas.com/display/TRAD/Trading+Component+Diagrams)
* [Missing Monads](https://git.nonprod.williamhill.plc/cdesautels/java-monads)
* [Fixture Mapping Service](https://conf.willhillatlas.com/display/TRAD/Fixture+Mapping+Service)
