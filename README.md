**Deprecated, see https://gitlab.com/williamhillplc/trading/pricing-standardisation/libraries/trading-onboarding**

Table of contents
=================

* [Table of contents](#table-of-contents)
* [OSX](#osx)
* [Ubuntu 16.04](#ubuntu-1604)
* [Useful links](#useful-links)

# OSX

The provided instructions are expected to run from OSX terminal, relative links are from this folder and configuration changes will override local customizations.

## Brew

1. Install XCode Command Line Tools:

    ~~~
    xcode-select --install
    ~~~

2. Install Brew Package Manager:

    ~~~
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    ~~~

3. Install required packages:

    ~~~
    brew tap homebrew/cask
    brew tap homebrew/cask-versions
    brew install docker docker-machine virtualbox intellij-idea git maven aws-okta awscli kubectl bluejeans slack terraform terragrunt helm helmsman
    ~~~

## Java

1. Install JDK:

    ~~~
    brew install openjdk@11 openjdk@8 openjdk
    ~~~
    
    **This will download the OpenJDK version. If you prefer, please use the [Oracle download page](https://www.oracle.com/technetwork/java/javase/downloads/index.html)**
    
2. Set JAVA_HOME:

    ~~~
    echo "export JAVA_8=/usr/local/opt/openjdk@8" >> ~/.bash_profile
    echo "export JAVA_11=/usr/local/opt/openjdk@11" >> ~/.bash_profile
    echo "export JAVA_15=/usr/local/opt/openjdk@15" >> ~/.bash_profile
    echo "alias java8='export JAVA_HOME=$JAVA_8'" >> ~/.bash_profile
    echo "alias java11='export JAVA_HOME=$JAVA_11'" >> ~/.bash_profile
    echo "alias java15='export JAVA_HOME=$JAVA_15'" >> ~/.bash_profile
    echo "java11" >> ~/.bash_profile
    source ~/.bash_profile
    ~~~

3. Download Nexus certificates to Java security folder:

    ~~~
    for cert in wh_iss.crt wh_root.crt wh_root_2034.crt wh_chain_sc1wnpresc03.crt wh_chain_sc1wnpresc04.crt;do curl --insecure --create-dirs --output $cert https://git.nonprod.williamhill.plc/profiles/profile_wh_sslcerts/raw/master/files/$cert; done
    ~~~

4. Install Nexus certificate to Java trust store:

    In Java 8
    
    ~~~
    for cert in wh_iss.crt wh_root.crt wh_root_2034.crt wh_chain_sc1wnpresc03.crt wh_chain_sc1wnpresc04.crt;do sudo ${JAVA_8}/bin/keytool -import -trustcacerts -storepass changeit -noprompt -alias $cert -file $cert; done
    ~~~

    In Java 11+

    ~~~
    for cert in wh_iss.crt wh_root.crt wh_root_2034.crt wh_chain_sc1wnpresc03.crt wh_chain_sc1wnpresc04.crt;do ${JAVA_11}/bin/keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -alias $cert -file $cert; done
    for cert in wh_iss.crt wh_root.crt wh_root_2034.crt wh_chain_sc1wnpresc03.crt wh_chain_sc1wnpresc04.crt;do ${JAVA_15}/bin/keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -alias $cert -file $cert; done
    ~~~

    For IntelliJ Importer

    ~~~
    for cert in wh_iss.crt wh_root.crt wh_root_2034.crt wh_chain_sc1wnpresc03.crt wh_chain_sc1wnpresc04.crt;do /Applications/IntelliJ\ IDEA.app/Contents/jbr/Contents/Home/bin/keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -alias $cert -file $cert; done
    ~~~

## Git

1. Configure Git user, change John Doe to your user:

    ~~~
    git config --global user.name "John Doe"
    ~~~

2. Configure Git email, change johndoe@example.com to your email:

    ~~~
    git config --global user.email johndoe@example.com
    ~~~

## SSH

1. Create SSH keys for your machine using the tutorial [here](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/).
2. Upload your SSH public key to your WH Gitlab profile [here](https://git.nonprod.williamhill.plc/profile/keys).
3. Update username in SSH configuration:

    ~~~
    sed -i '' "s/\[username\]/$(whoami)/g" config
    ~~~

4. Update users SSH configuration:

    ~~~
    cp config ~/.ssh/config
    ~~~
5. Add SSH to OSX keychain
    
    ~~~
    ssh-add -AK ~/.ssh/[private_key]
    ~~~

## Docker

1. Update Docker daemon.json (if folder doesn't exist, please run docker):

    ~~~
    docker-machine create --driver virtualbox default
    docker-machine restart
    eval "$(docker-machine env default)"
    docker run hello-world
    ~~~

2. Log into the WH Docker registry with AD credentials:

    ~~~
    docker login docker-registry.prod.williamhill.plc
    ~~~

## Maven

1. Update Maven settings.xml:

    ~~~
    mkdir -p ~/.m2
    curl -k https://git.nonprod.williamhill.plc/trading_development/common/gitlab-ci/raw/master/maven-settings.xml > ~/.m2/settings.xml
    ~~~

## IntelliJ

1. Install Checkstyle and Sonar plugins, these will give you helpful warnings in your editor:
    * Navigate to: **IntelliJ > Preferences > Plugins**, install Checkstyle-IDEA and SonarLint.
2. Configure Checkstyle plugin:
    * Navigate to: **IntelliJ > File > Other Settings > Default Settings > Other Settings > Checkstyle**, add configuration file `trading-style-v8.XX.xml` the version should match your checkstyle.
3. Configure Checkstyle plugin:
    * Navigate to: **IntelliJ > File > Other Settings > Default Settings > Other Settings > Checkstyle**, change scan scope to include source and tests.
4. Configure Maven plugin to download sources:
    * Navigate to: **IntelliJ > File > Other Settings > Default Settings > Build > Maven > Importing**, automatically download sources and documentation.
5. Configure Maven plugin to run using installed JRE:
    * Navigate to: **IntelliJ > File > Other Settings > Default Settings > Build > Maven > Runner**, change JRE to installed.
6. Configure Maven compiler plugin to enable annotation processing:
    * Navigate to: **IntelliJ > File > Other Settings > Default Settings > Build > Maven > Compiler > Annotation Processors**, enable annotation processing.
7. Configure SonarLing plugin:
    * Navigate to: **IntelliJ > Preferences > Other Settings > Sonar Lint General Settings**, Add SonarQube connection by URL `https://sonarqube.infosec.aws-eu-west-1.prod.williamhill.plc` and AD credentials.


## Archi ##

1. Follow the setup guide [here](https://conf.willhillatlas.com/x/TgkpEg).

# Ubuntu 16.04

## Changing Swappiness

The offical Ubuntu [swap FAQ](https://help.ubuntu.com/community/SwapFaq) states;
> 'The default setting in Ubuntu is swappiness=60. Reducing the default value of swappiness will probably improve overall performance for a typical Ubuntu desktop installation. **A value of swappiness=10 is recommended**'

1. To check the swappiness value
   ~~~~
   cat /proc/sys/vm/swappiness
   ~~~~

2. Edit the configuration file with your favorite editor:  
   ~~~~
   sudo nano /etc/sysctl.conf
   ~~~~

3. Search for vm.swappiness and change its value. If vm.swappiness does not exist, add it to the end of the file like so:
   ~~~~
   vm.swappiness=10
   ~~~~

4. Save the file and reboot.

## Docker

1. Download and install Docker (depends on which version of ubuntu you have installed)
   ~~~~
   sudo apt-get install docker-ce=17.06.0~ce-0~ubuntu
   ~~~~

2. Add your Docker DNS details to `/etc/docker/daemon.json` (DNS details on ubuntu: ```settings -> network -> network-connection``` DNS)
   ~~~~
   "dns": ["10.120.193.238", "10.120.193.239"]
   ~~~~

3. In order to download WilliamHill Docker images you must add the following Docker registry details to `/etc/default/docker`
   ~~~~
   DOCKER_OPTS="--insecure-registry docker-registry.dev.williamhill.plc --insecure-registry docker-registry-dev.local --insecure-registry docker-registry.prod.williamhill.plc --disable-legacy-registry=false"
   ~~~~

4. Updating `docker.service` by running `sudo nano /lib/systemd/system/docker.service` and
   ~~~
   Replacing:
   ExecStart=/usr/bin/dockerd -H fd://
   With:
   EnvironmentFile=-/etc/default/docker
   ExecStart=/usr/bin/dockerd $DOCKER_OPTS -H fd://
   ~~~

5. Make sure to restart Docker
   ~~~~
   sudo service docker stop
   sudo systemctl daemon-reload
   sudo service docker start
   ~~~~

6. You can also restart Docker by running
   ~~~
   sudo systemctl restart docker
   ~~~

## Maven
Install maven and update `/etc/maven/settings.xml` with

~~~~
    <server>
        <id>nexus-releases</id>
        <username>deploy-user</username>
        <password>d3v3lopers</password>
    </server>
    <server>
        <id>nexus-snapshots</id>
        <username>deploy-user</username>
        <password>d3v3lopers</password>
    </server>
~~~~

~~~~
    <mirror>
        <id>maven.apps.local</id>
        <name>William Hill Maven Repository and proxy</name>
        <url>http://maven.apps.local:8082/nexus/content/groups/public</url>
        <mirrorOf>*,!sonar</mirrorOf>
        </mirror>
     <mirror>
        <id>maven.apps.local</id>
        <name>William Hill Maven Repository and proxy</name>
        <url>http://trd-nexus.dev.williamhill.plc:8081/nexus/content/groups/public</url>
        <mirrorOf>*,!sonar</mirrorOf>
    </mirror>
~~~~

Add the following to `/etc/hosts`

~~~~
10.1.28.8 maven.apps.local nexus
10.210.200.25 gitlab.williamhill-dev.local
~~~~

## IntelliJ

If using IntelliJ (2018+) you need to update the default Settings to always use your local configuration of Maven.

1.  Navigate to `File -> Other Settings -> Default Settings`

2.  Select **Build, Execution, Deployment**

3.  Select **Build Tools**

4.  Select **Maven**

5.  From **Maven home directory** select dropdown option `/usr/share/maven`

6.  Tick the **Override** option box and insert `/etc/maven/settings.xml`

7.  Click **Apply** and then **OK** buttons.

## SSH

* To simplify SSHing into different boxes I would recommend copying the [config file](https://git.nonprod.williamhill.plc/Junayedmizan/NewStarterSetup/blob/master/config) into your `.ssh` directory - `/home/$USER/.ssh`
* Replace `${username}` with your WilliamHill username. For example `mibell`, not `${mibell}`


# Useful Links

## Dev

1. GitLab - Access the internal web based git repository manager

    Current:

    ~~~~
    https://git.nonprod.williamhill.plc/
    ~~~~    

    Legacy:

    ~~~~
    http://gitlab.williamhill-dev.local/
    ~~~~

    To access gitlab repos ssh key must be added: [how to add ssh key](https://git.nonprod.williamhill.plc/help/ssh/README)

2. Jira

    ~~~~
    https://jira.willhillatlas.com/secure/Dashboard.jspa
    ~~~~

3. Jenkins

    ~~~~
    http://10.1.74.206:8080/
    ~~~~


4. Splunk - Accessing server logs for the different services and environment. Just replace ```env``` with the environment you are after e.g. ```https://clp.pp1.williamhill.plc```

    ~~~~
    https://clp.env.williamhill.plc
    ~~~~

5. Current release process

    ~~~~
    https://conf.willhillatlas.com/display/TRAD/Sports+Stack+Release+Process
    ~~~~

6. Global Trading Platform RabbitMQ settings- Accessing server logs for the different services and environment. Just replace ```env``` with the environment you are after e.g. ```http://rabbit-ui.trading-services.pp1.williamhill.plc/```

    ~~~~
    http://rabbit-ui.trading-services.env.williamhill.plc/
    ~~~~  

7. Global Trading Platform

    ~~~~
    http://gtp-ui.trading-services.pp1.williamhill.plc/
    ~~~~  

8. Global Trading Platform: In-play UI

    ~~~~
    http://football-inplay-ui.trading-services.pp1.williamhill.plc
    ~~~~

9. William Hill Wiki - This is where teams can collaborate and share knowledge. This would be the go to place **first** if you don't understand/have any questions.

    ~~~~
    https://conf.willhillatlas.com
    ~~~~


## Admin

1. Slack - To access web based version of Slack

    ~~~~
    https://slack.com
    ~~~~

2. Accessing Outlook, One Drive, Word, Excel and other microsoft products used within William Hill

    ~~~~
    https://www.office.com
    ~~~~        

3. Time Tracking

    ~~~~
    https://login.dovico.com/#Login
    ~~~~

4. Service Now - If you need to make `IT Orders` (e.g. Licence for IntelliJ IDE).

    ~~~~
    https://williamhill.service-now.com/navpage.do
    ~~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For Software orders use `Cost Centre Code: LO0109`, Hardware orders use `Cost Centre Code: LO0187`.

5. Employee News Channel

    ~~~~
    https://go2.williamhill.com
    ~~~~

6. William Hill corporateperks

    ~~~~
    https://williamhill.corporateperks.com/index/index/showftu/1
    ~~~~

7. Team calender

    ~~~~
    https://conf.willhillatlas.com/calendar/mycalendar.action
    ~~~~    


## Booking & Recording Holidays

- To book holiday please send an email to your Project Manager, Technical Lead and the Development Manager ([Chris Noble](chris.noble@williamhill.com)).
- Please try and provide as much notice as possible to avoid any issues.
- If you do not receive any feedback on any issues then you can assume the holiday is fine.
- Also add your time out of the office to the team [calendar](https://conf.willhillatlas.com/calendar/mycalendar.action) in the Wiki:


All absence including holidays and bank holidays must be recorded in Dovico. Chris Raus has kindly provided a table to describe the different types of absence in Dovico. All holidays including bank holidays should be recorded as Annual Leave and your Birthday Day should be recorded as Authorised Absence.

If you are unsure please check before recording in [Dovico](https://login.dovico.com/#Login):

| Category                           | Comments                                                                                                       |
|------------------------------------|----------------------------------------------------------------------------------------------------------------|
| Annual Leave (zero charge)         | Used for personal holidays within the employees allowance, including Bank Holidays                             |
| Authorised Absence                 | Absence approved by the employees line manager that is not A/L. For example: birthday days off, funerals etc.. |
| Lieu Time (zero charge)            | Days to be taken in place of overtime, to be agreed with the employees line manager                            |
| Mat / Pat Leave                    | Maternity / paternity leave                                                                                    |
| Parental Leave                     | Unpaid leave an employee can take outside of the entitled maternity / paternity leave                          |
| Sickness                           | Sick leave                                                                                                     |
| Unauthorised Absence (zero charge) | Unpaid absence e.g. from not calling in sick for an extended time off                                          |

## Firewall
If you have problems connecting to jump hosts, its probably because of firewall rules.

To get access to jump hosts and other enviornments, send an email to `servicedesk@williamhill.co.uk`. Include your line manager in the email so that he can approve the request.

Make sure to include your current ip address.

## Environment Access

Make sure your name has been added to the [GTP Access list](https://conf.willhillatlas.com/display/TRAD/GTP+Access)
